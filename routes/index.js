var test_directory = 'public/images/test';
var reference_directory = 'public/images/reference';

var express = require('express');
var router = express.Router();
var multer = require('multer');
var test_upload = multer({ dest : test_directory });
var reference_upload = multer({ dest : reference_directory });
var fs = require('fs');
var request = require('superagent');

var stats = require('../model/stats.js');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* Image uploads */
router.post('/test_upload', test_upload.single('image'), function(req, res){
  console.log('Got ' + req.file.filename);
  res.json({filename : req.file.filename, status: 200});
});

/* Image retrieval */
router.get('/test_list', function(req, res) {
  fs.readdir(test_directory, function(err, files) {
    if(err)
      res.json({files : [], msg : err, status : 500});
    else {
      res.json({files : files, msg : 'ok', status : 200});
    }
  });
});

/* Image match */
router.post('/match', function(req, res) {
  console.log("Got " + req.body.path);
  request
    .post('http://81.99.174.76:5000/match/' + req.body.path)
    .end(function(err, response){
      if (err) {
        console.log('Oh no! error');
        console.log(err);
        console.log("res.ok? " + res.ok);
      } else {
        console.log("done");
        console.log(response);
      }
      console.log(response.body);
      res.send(response.body);
    });
});

router.post('/matchquality', function(req, res) {
  console.log('body');
  console.log(req.body);
  var accurate = req.body.accurate;
  var time = req.body.time;
  console.log("Accurate : " + accurate.toString() + " Time : " + time.toString());
  //stats.add_request(accurate, time, function(err, data) {
    //if(err) {
        //console.log(err);
        //res.send({ error : err, status : 500 });
    //}
    //else {
      //res.send({ stats : data, status : 200 });
    //}
  //});
  res.send({ stats : data, status : 200 });
});

module.exports = router;
