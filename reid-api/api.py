from flask import request, url_for, Response, jsonify
from flask.ext.api import FlaskAPI, status, exceptions
from werkzeug import secure_filename
from flask.ext.cors import CORS
import os
import time

# REID imports
from engine.Objects import image, patch
from engine.Ranker import Ranker

app = FlaskAPI(__name__)
CORS(app)

# Constants
TEST_FOLDER = '/home/mrchrondank/Code/Web/git/reid/public/images/test/'
REFERENCE_FOLDER = '/home/mrchrondank/Code/Web/git/reid/public/images/reference/'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'bmp'])
ALPHA = 1
THETA = 1.25

# Globals
reference_images = []

# Config
app.config['REFERENCE_FOLDER'] = REFERENCE_FOLDER
app.config['TEST_FOLDER'] = TEST_FOLDER
app.config['ALLOWED_EXTENSIONS'] = ALLOWED_EXTENSIONS

# Methods

# REID init
def reid_init():
    print("Initialising REID engine")
    reference_filenames = os.listdir(app.config['REFERENCE_FOLDER'])
    print("Found " + str(len(reference_filenames)) + " reference images")
    print("Processing reference images")
    # process reference images
    for fn in reference_filenames:
        path = os.path.join(app.config['REFERENCE_FOLDER'], fn)
        img = image(path)
        img.prepare(10, 4)
        reference_images.append(img)
    print("Processed reference images")
    print("Got " + str(len(reference_images)) + " reference images")

def match_image(test_path):
    start = time.clock()
    test_image = image(test_path)
    test_image.prepare(10, 4)

    # Ranker
    rk = Ranker(test_image, reference_images, 10, 4, 10, ALPHA, THETA)
    rankings, correspondence = rk.calculate_rankings()
    end = time.clock()
    req_time = end-start
    return rankings[-1], req_time, correspondence

# Check if filename is allowed
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

# Routes

# Upload an image to the test set
@app.route('/test_upload', methods=['POST'])
def test_upload():
    if request.method == 'POST':
        file = request.files['image']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            path = os.path.join(app.config['TEST_FOLDER'], filename)
            file.save(path)
            return Response(status=200, mimetype='application/json')
        else:
            return Response(status=500, mimetype='application/json')

# Upload an image to the reference set
@app.route('/reference_upload', methods=['POST'])
def reference_upload():
    if request.method == 'POST':
        file = request.files['image']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            path = os.path.join(app.config['REFERENCE_FOLDER'], filename)
            file.save(path)
            return Response(status=200, mimetype='application/json')
        else:
            return Response(status=500, mimetype='application/json')

# Get a match for the given test image
@app.route('/match/<path>', methods=['POST'])
def match(path):
    if request.method == 'POST':
        path = os.path.join(app.config['TEST_FOLDER'], path)
        print("Test image : " + str(path))
        result, req_time, correspondence = match_image(path)
        print("GOT FOLLOWING RESULT FOR IMAGE " + path)
        print(result)
        js_dict = { "path" : result[0].split("/")[-1], "score" : result[1], "time" : req_time, "correspondence" : correspondence }
        return jsonify(**js_dict)

# Main loop
if __name__ == '__main__':
    reid_init()
    app.run(
        host="0.0.0.0",
        port=int("5000"),
        debug=True
    )
