import numpy as np
import math

class NearestNeighbour(object):

    def __init__(self, theta, reference_patches):
        self.patches = reference_patches
        self.theta = theta

	def similarity(self, distance):
		dist_sq = pow(distance, 2)
		theta_sq = pow(self.theta, 2)
		return exp(-1 * (dist_sq/(2*theta_sq)))

    def get(self, test_patch):

        max_similarity = 0
        max_similarity_index = -1

        for patch_index, reference_patch in enumerate(self.patches):
            # calculate the distance between the descriptor of the test_patch and the reference_patch
            ref_descriptor = reference_patch.descriptor
            test_descriptor = test_patch.descriptor
            dist = np.linalg.norm(ref_descriptor-test_descriptor)
            self.patches[patch_index].distance = dist
            # calc similarity_score
            dist_sq = pow(dist, 2)
            theta_sq = pow(self.theta, 2)
            similarity = math.exp(-1 * (dist_sq/(2*theta_sq)))

            if similarity > max_similarity:
                max_similarity = similarity
                max_similarity_index = patch_index

        most_similar_patch = self.patches[max_similarity_index]
        most_similar_patch.similarity = max_similarity
        #print("max sim " + str(max_similarity))

        return most_similar_patch
