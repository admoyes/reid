import cv2
from Descriptor import descriptor
from math import pow, exp

class image(object):

	def __init__(self, fn):
		self.filename = fn
		self.data = cv2.imread(fn) # cv2 image
		self.patches = [] # will be a 2d array (columns x rows)
		self.salience_map = None

	def __str__(self):
		print("image:")
		print("--filename: " + str(self.filename))
		print("--data: " + str(type(self.data)) + " " + str(len(self.data)))
		return ""

	def get_row(self, row):
		return self.patches[row][:]

	def prepare(self, patch_size, grid_step):
		h, w, _ = self.data.shape # get height and width of self.data
		patches = []
		for r in range(0, (h-patch_size), grid_step):
			descriptor_row = []
			for c in range(0, (w-patch_size), grid_step):
				patch_data = self.data[r:r+patch_size, c:c+patch_size]
				p = patch(self.filename, patch_data, len(patches), len(descriptor_row))
				p.calculate_descriptor()
				descriptor_row.append(p)
			patches.append(descriptor_row)
		self.patches = patches

class patch(object):

	def __init__(self, fn, data, row, column):
		self.filename = fn
		self.data = data
		self.row = row # this will be the descriptor row, i.e. it's out of 30 not 128
		self.column = column # this will be the descriptor column, i.e. it's out of 10 not 48
		self.descriptor = None # 672-dimension feature vector
		self.distance = None # euclidean distance
		self.similarity = None # similarity score
		self.xnn = [] # best patch from each image in the reference set

	def __str__(self):
		print("patch:")
		print("--filename: " + str(self.filename))
		print("-------row: " + str(self.row))
		print("----column: " + str(self.column))
		print("-------xnn: " + str(len(self.xnn)))
		print("similarity: " + str(self.similarity))
		print("--distance: " + str(self.distance))
		return ""

	def calculate_descriptor(self):
		ds = descriptor()
		self.descriptor = ds.get(self.data, norm=True)

	def scoreKNN(self):
		# return the len(self.xnn)/2'th neighbour
		index = int(len(self.xnn)/2)
		xnn_sorted = sorted(self.xnn, key=lambda x: x.distance, reverse=True)
		return xnn_sorted[index].distance

	def sim(self, bandwidth):
		dist_sq = pow(self.distance, 2)
		theta_sq = pow(bandwidth, 2)
		return exp(-1 * (dist_sq/(2*theta_sq)))

	def as_json(self, recursive=False):
		print(str(self))
		json = {}
		json["filename"] = self.filename
		json["row"] = self.row
		json["column"] = self.column
		json["distance"] = float(self.distance)
		if self.similarity is not None:
			json["similarity"] = float(self.similarity)
		else:
			json["similarity"] = float(0)

		xnn = {}
		if recursive:
			for x in range(0, len(self.xnn)):
				xnn[x] = self.xnn[x].as_json()

		json["xnn"] = xnn

		return json
