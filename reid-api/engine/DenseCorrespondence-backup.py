import numpy as np
import cv2
from Descriptor import descriptor
from Objects import *

class DenseCorrespondence(object):

	def __init__(self, img1, img2, patch_size, grid_step, theta):
		self.patch_size = patch_size
		self.grid_step = grid_step
		self.theta = theta
		self.img1 = img1
		self.img2 = img2

	def get(self, N, verbose=False):
		# returns the dense correspondence between two images
		for row in range(0, len(self.img1.patches)): # len(self.img1.patches) should return the number of rows
			# now get all patches in that row from image two
			row_start = row - 2
			row_end = row + 3
			if row_start < 0:
				row_start = 0
				row_end = 5
			if row_end >= (len(self.img1.patches)-3):
				row_end = len(self.img1.patches)
				row_start = row_end - 5

			row_patches = []
			for r in range(row_start, row_end):
				row_patches = row_patches + self.img2.get_row(r)

			#print(str(row_start) + " " + str(row) + " " + str(row_end))

			# next we need to go through each patch of img1 in the current row
			#print("row_patches " + str(len(row_patches)))
			for column in range(0, len(row_patches)/5):
				#print("row_patches: " + str(len(row_patches)))
				#print("row: " + str(row) + " column: " + str(column))
				current_patch = self.img1.patches[row][column]

				# so now we have the patch from img1 and access to the corresponding search row in img2
				# next we do a KNN for current_patch in row_patches

				# prepare the data
				row_descriptors = np.array([p.descriptor for p in row_patches]).astype(np.float32) # aka train data
				_, descriptor_length = row_patches[0].descriptor.shape
				row_descriptors = row_descriptors.reshape((len(row_patches), descriptor_length)).astype(np.float32)
				row_labels = np.arange(len(row_patches)).astype(np.float32)
				#print("row descriptors shape: " + str(row_descriptors.shape))
				# train the classifier
				knn = cv2.KNearest()
				knn.train(row_descriptors, row_labels)

				# get the NN
				ret, results, neighbours, dists = knn.find_nearest(current_patch.descriptor, N)

				# get the results
				the_best_match = int(neighbours[0][0]) # the_best_match is now the index of the best match
				# we may need to change this but here it goes
				the_best_dist = dists[0][0]

				similarities = []
				for i in range(0, len(neighbours[0])):
					patch_index = int(neighbours[0][i])
					dist = dists[0][i]
					p = row_patches[patch_index]
					p.distance = dist
					sim = p.sim(self.theta)
					similarities.append([patch_index, dist, sim])

				# let's get the actual best patch from row_patches
				the_best_patch = row_patches[the_best_match]
				the_best_patch.distance = the_best_dist
				self.img1.patches[row][column].xnn.append(the_best_patch)

				if verbose:
					# let's draw some rectangles
					# start by drawing the current_patch on img1
					img1_data = self.img1.data.copy()
					cv2.rectangle(img1_data, (current_patch.column*self.grid_step,current_patch.row*self.grid_step), ((current_patch.column*self.grid_step)+self.patch_size, (current_patch.row*self.grid_step)+self.patch_size), (255,255,0), 1)

					# now let's draw the current best match on img2
					img2_data = self.img2.data.copy()
					cv2.rectangle(img2_data, (the_best_patch.column*self.grid_step, the_best_patch.row*self.grid_step), ((the_best_patch.column*self.grid_step)+self.patch_size,(the_best_patch.row*self.grid_step)+self.patch_size), (255,255,0), 1)

					cv2.rectangle(img2_data, (0, row_start), (48, row_end), (0, 0, 255), 1)

					# now hstack the two images
					output = np.hstack((img1_data, img2_data))
					cv2.imshow("best match", output)
					cv2.waitKey(0)
		return self.img1
