# This will test how well the correspondence can handle two identical images (it should be exact)

import os
from Objects import image, patch 
from DenseCorrespondence import *

# CONSTANTS
PATCH_SIZE = 10
GRID_STEP = 4

img1_path = "/home/mrchrondank/Datasets/001_45.bmp"
img2_path = "/home/mrchrondank/Datasets/001_90.bmp" 

img1 = image(img1_path)
img2 = image(img2_path)

img1.prepare(PATCH_SIZE, GRID_STEP)
img2.prepare(PATCH_SIZE, GRID_STEP)

dc = DenseCorrespondence(img1, img2, PATCH_SIZE, GRID_STEP)
dc.get(1, True)
