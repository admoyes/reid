import numpy as np
import cv2
from Descriptor import descriptor
from Objects import *
from NearestNeighbour import NearestNeighbour

class DenseCorrespondence(object):

	def __init__(self, img1, img2, patch_size, grid_step, theta):
		self.patch_size = patch_size
		self.grid_step = grid_step
		self.theta = theta
		self.img1 = img1
		self.img2 = img2

	def get(self, N, verbose=True):
		# returns the dense correspondence between two images
		for row in range(0, len(self.img1.patches)): # len(self.img1.patches) should return the number of rows
			# now get all patches in that row from image two
			row_start = row - 2
			row_end = row + 3
			if row_start < 0:
				row_start = 0
				row_end = 5
			if row_end >= (len(self.img1.patches)-3):
				row_end = len(self.img1.patches)
				row_start = row_end - 5

			row_patches = []
			for r in range(row_start, row_end):
				row_patches = row_patches + self.img2.get_row(r)

			#print(str(row_start) + " " + str(row) + " " + str(row_end))

			# next we need to go through each patch of img1 in the current row
			#print("row_patches " + str(len(row_patches)))
			for column in range(0, len(row_patches)/5):
				#print("row_patches: " + str(len(row_patches)))
				#print("row: " + str(row) + " column: " + str(column))
				test_patch = self.img1.patches[row][column]

				knn = NearestNeighbour(self.theta, row_patches)
				the_best_patch = knn.get(test_patch)
				#print("best patch by knn")
				#print(str(the_best_patch))


				# let's get the actual best patch from row_patches
				self.img1.patches[row][column].xnn.append(the_best_patch)

				if verbose:
					# let's draw some rectangles
					# start by drawing the current_patch on img1
					img1_data = self.img1.data.copy()
					cv2.rectangle(img1_data, (test_patch.column*self.grid_step,test_patch.row*self.grid_step), ((test_patch.column*self.grid_step)+self.patch_size, (test_patch.row*self.grid_step)+self.patch_size), (255,255,0), 1)

					# now let's draw the current best match on img2
					img2_data = self.img2.data.copy()
					cv2.rectangle(img2_data, (the_best_patch.column*self.grid_step, the_best_patch.row*self.grid_step), ((the_best_patch.column*self.grid_step)+self.patch_size,(the_best_patch.row*self.grid_step)+self.patch_size), (255,255,0), 1)

					cv2.rectangle(img2_data, (0, row_start), (48, row_end), (0, 0, 255), 1)

					# now hstack the two images
					output = np.hstack((img1_data, img2_data))
					cv2.imshow("best match", output)
					cv2.waitKey(0)
		return self.img1

	def get_salience_map(self, img):
		salience_map = np.zeros((128, 48))
		patches = img.patches
		for r in patches:
			for c in r:
				patch = c
				score = patch.distance
				row = patch.row * 4
				column = patch.column * 4
				salience_map[row:row+10, column:column+10] += score

		for r in range(0, 128):
			salience_map[r] /= np.max(salience_map[r])

		#salience_map /= np.linalg.norm(salience_map, axis=(0))

		# blur map
		kernal = np.ones((5,5), np.float32) / 25
		dst = cv2.filter2D(salience_map, -1, kernal)

		cv2.imshow("salience", dst)
		cv2.waitKey(0)
		return salience_map
