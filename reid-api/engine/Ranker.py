import math
from DenseCorrespondence import *
import copy

class Ranker(object):

	def __init__(self, test_image, train_images, patch_size, grid_step, N, alpha, theta, verbose=False):
		print("ranker init")
		# test_image : image() object
		# train_images: array of image() objects
		self.test_image = test_image
		self.train_images = train_images
		self.patch_size = patch_size
		self.grid_step = grid_step
		self.N = N
		self.alpha = alpha
		self.theta = theta
		self.verbose = verbose
		self.build_correspondences()

	def build_correspondences(self):
		print("build_correspondences")
		print("got " + str(len(self.train_images)) + " train images")
		# go through each train image
		# calculate the correspondence between the current image and all other
		# images in the reference set
		train_correspondences = []

		image_pool = copy.deepcopy(self.train_images)
		image_pool.append(self.test_image)
		print("got " + str(len(image_pool)) + " images in the pool")

		for img1 in image_pool:
			for img2 in image_pool:
				# ensure we are not comparing identical images
				if img1.filename != img2.filename:
					dc = DenseCorrespondence(img1, img2, self.patch_size, self.grid_step, self.theta)
					img1 = dc.get(self.N, False)
					#print("img1 xnn " + str(len(img1.patches[0][0].xnn)))
			train_correspondences.append(img1)
		self.test_correspondence = train_correspondences.pop()
		self.train_correspondences = train_correspondences

		# now build correspondence between test image and each train image
		#img1 = self.test_image
		#for img2 in self.train_images:
		#	dc = DenseCorrespondence(img1, img2, self.patch_size, self.grid_step, self.theta)
		#	img1 = dc.get(self.N, False)
		#self.test_correspondence = img1


	def calculate_rankings(self):
		print("calculate_rankings")
		print("train_correspondences (" + str(len(self.train_correspondences)) + ")")
		print("test filename: " + self.test_correspondence.filename)
		rankings = {}
		for index in range(0, len(self.train_correspondences)):
			img = self.train_correspondences[index]
			score = 0
			for row_index, row in enumerate(img.patches):
				for column_index, column in enumerate(row):
					# train patch = img[row][column].xnn[index]
					test_patch = self.test_correspondence.patches[row_index][column_index]
					train_patch = test_patch.xnn[index]
					scoreKNN_a = test_patch.scoreKNN()
					scoreKNN_b = train_patch.scoreKNN()
					#print(type(train_patch))
					#print("theta " + str(self.theta))
					#similarity_score = train_patch.similarity
					#similarity_score = train_patch.sim(self.theta)
					similarity_score = train_patch.similarity
					#print("train_patch.distance " + str(train_patch.distance))
					#print("train_patch.similarity " + str(similarity_score))
					#print("train_patch.sim(self.theta) " + str(train_patch.sim(self.theta)))
					numerator = (scoreKNN_a * similarity_score * scoreKNN_b)
					denominator = float(self.alpha) + abs(scoreKNN_a-scoreKNN_b)

					#if(denominator == float(0)):
					#	denominator = float(1)
					score += (numerator / denominator)
					#print("scoreKNN_a " + str(scoreKNN_a))
					#print("a fn : " + test_patch.filename)
					#print("scoreKNN_b " + str(scoreKNN_b))
					#print("b fn : " + train_patch.filename)
					#print("similarity_score " + str(similarity_score))
			rankings[img.filename] = score
		rankings = sorted(rankings.items(), key=lambda x:x[1])
		print("test fn " + self.test_correspondence.filename)
		for r in rankings:
			print(r)
		return rankings, self.image_as_iterable(self.test_correspondence)

	def image_as_iterable(self, img):
		patches = []
		print("image " + img.filename)
		print("	patches " + str(len(img.patches)) + "x" + str(len(img.patches[0])))
		for r in range(0, len(img.patches)):
			row = []
			for c in range(0, len(img.patches[r])):
				print("r" + str(r) + " c" + str(c))
				row.append(img.patches[r][c].as_json(True))
			patches.append(row)

		return { "filename" : img.filename, "patches" : patches }
