# Produces a 672-feature descriptor for a patch.
import numpy as np
import cv2

class descriptor(object):

	def get(self, patch, norm=True):
		patch_lab = cv2.cvtColor(patch, cv2.COLOR_BGR2LAB)
		patch_lab_7 = np.resize(patch_lab, (7,7,3))
		patch_lab_5 = np.resize(patch_lab, (5,5,3))

		# HISTOGRAMS

		# L
		hist_l = self.histogram(patch_lab[:,:,0], 32, [0,255], norm)
		hist_l_7 = self.histogram(patch_lab_7[:,:,0], 32, [0,255], norm)
		hist_l_5 = self.histogram(patch_lab_5[:,:,0], 32, [0,255], norm)

		# A
		hist_a = self.histogram(patch_lab[:,:,1], 32, [0,255], norm)
		hist_a_7 = self.histogram(patch_lab_7[:,:,1], 32, [0,255], norm)
		hist_a_5 = self.histogram(patch_lab_5[:,:,1], 32, [0,255], norm)

		# B
		hist_b = self.histogram(patch_lab[:,:,2], 32, [0,255], norm)
		hist_b_7 = self.histogram(patch_lab_7[:,:,2], 32, [0,255], norm)
		hist_b_5 = self.histogram(patch_lab_5[:,:,2], 32, [0,255], norm)

		histogram_results = np.concatenate([hist_l, hist_l_7, hist_l_5, hist_a, hist_a_7, hist_a_5, hist_b, hist_b_7, hist_b_5]).flatten()

		# SIFT
		sift_l = self.sift(patch_lab[:,:,0], norm)
		sift_a = self.sift(patch_lab[:,:,1], norm)
		sift_b = self.sift(patch_lab[:,:,2], norm)

		sift_results = np.concatenate([sift_l, sift_a, sift_b]).flatten()

		dColorSIFT = np.concatenate([histogram_results, sift_results])
		dColorSIFT_length = dColorSIFT.shape[0]
		return np.array(dColorSIFT).reshape((1,dColorSIFT_length)).astype(np.float32)

	def histogram(self, patch, bins, d_range, norm):
		data = patch.flatten()
		hist, bins = np.histogram(data, bins, d_range)

		if norm:
			n = np.linalg.norm(hist)
			hist = hist/n
		return hist

	def sift(self, patch, norm):
		sift = cv2.SIFT(nfeatures=3)
		kp, des = sift.detectAndCompute(patch, None)
		if not des is None:
			d = des[0]

			if norm:
				n = np.linalg.norm(d)
				d /= n
			return d.reshape((1,128))
		else:
			return np.zeros((1,128))
