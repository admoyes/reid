import os
from Objects import *
from Ranker import *
import copy

test_path = "/home/mrchrondank/Datasets/VIPeR/test/"
train_path = "/home/mrchrondank/Datasets/VIPeR/train/" # points to directory
train_images = []
train_filenames = os.listdir(train_path)
for fn in train_filenames:
	path = os.path.join(train_path, fn)
	img = image(path)
	img.prepare(10, 4)
	train_images.append(img)

# now we have all our train images.
# let's load the test image
test_image = image(test_path)
test_images = []
test_filenames = os.listdir(test_path)
for fn in test_filenames:
	path = os.path.join(test_path, fn)
	img = image(path)
	img.prepare(10, 4)
	test_images.append(img)

results_test = []
results_train = []
alpha = 0.125
theta = 1.25

for img in test_images:
	train_images_copy = copy.deepcopy(train_images)
	rk = Ranker(img, train_images_copy, 10, 4, 10, alpha, theta)
	rankings = rk.calculate_rankings()
	results_test.append([img.filename, rankings[-1]])

#for img in train_images:
#	rk = Ranker(img, test_images, 10, 4, 10, alpha, theta)
#	rankings = rk.calculate_rankings()
#	results_train.append([img.filename, rankings[-1]])


total = len(results_test)
correct = 0
for res in results_test:
	print(res)
	if res[0].split("/")[-1].split("_")[0] == res[1][0].split("/")[-1].split("_")[0]:
		correct += 1

#for index, res in enumerate(results_test):
#	res_test = res
#	res_train = results_train[index]
#
#	res_test_test_fn = res_test[0].split("/")[-1].split("_")[0]
#	res_test_match_fn = res_test[1][0].split("/")[-1].split("_")[0]
#
#	res_train_test_fn = res_train[0].split("/")[-1].split("_")[0]
#	res_test_match_fn = res_test[1][0].split("/")[-1].split("_")[0]
#
#	print(res_test_test_fn)
#	print(res_test_match_fn)
#	print(res_train_test_fn)
#	print(res_test_match_fn)
#
print(str(float(correct)/float(total)*100) + "% Correct.")
