gulp = require('gulp');
stylus = require('gulp-stylus');
webpack = require('gulp-webpack');
webpack_config = require('./webpack.config.js');

gulp.task('stylus', function () {
  return gulp.src('stylus/*.styl')
    .pipe(stylus())
    .pipe(gulp.dest('public/stylesheets'))
});

gulp.task('webpack', function () {
  return gulp.src('build/components/main.js')
    .pipe(webpack(webpack_config))
    .pipe(gulp.dest('public/javascripts/'))
});

gulp.task('watch', function () {
  gulp.watch('stylus/*', ['stylus']);
  gulp.watch('build/components/**/*.jsx', ['webpack']);
})

gulp.task('default', ['stylus', 'webpack', 'watch'])
