import React from 'react';

class Homepage extends React.Component {
  render () {
    return (
      <div className='content overflow-y'>
        <h1>Person RE-IDentification</h1>
        <p>Reid is a person recognition system written in <a href='https://www.python.org/'>Python</a> using the <a href='http://opencv.org/'>OpenCV</a> library.</p>
        <p>The system is my implementation of a <a href='http://bit.ly/22APpL7'>paper</a> by Rui Zhao, Wanli Ouyang and Xiaogang Wang from The Chinese University of Hong Kong.</p>
        <p></p>
        <h2>How Does It Work?</h2>
        <p>
          Reid works by comparing a test image to its own pool of images. It scans over each of the images in small patches, looking at colour histograms and SIFT features
          to build up a description of each patch. It then compares this patch to patches in each image in its pool. If there are 10 images in the image pool then each patch
          in the test image will be matched to 10 other patches; 1 from each image in the pool.

          The similarity between these matches is quantified.

        </p>
      </div>
    );
  }
}

export default Homepage;
