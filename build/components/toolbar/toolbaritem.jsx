import React from 'react';

class ToolbarItem extends React.Component {
    constructor (props) {
      super(props);
    }
    render () {
      var cn;
      this.props.selected ? cn='toolbaritem selected'  : cn = 'toolbaritem';
      return (
          <div className={cn} onClick={this.props.click}>
            <i className={this.props.icon}></i>
            <div className='sub'>{this.props.text}</div>
          </div>
        );
    }
}

export default ToolbarItem;
