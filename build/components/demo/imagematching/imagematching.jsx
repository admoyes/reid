import React from 'react';
import ToolbarItem from '../../toolbar/toolbaritem.jsx';
import Toolbar from '../../toolbar/toolbar.jsx';
import Button from '../../button/button.jsx';
import request from 'superagent';

class ImageMatching extends React.Component {
  constructor (props) {
    super(props);
    this.state = { action : 'none', matched : null, correspondence : null };
  }

  setActivePage (i) {
    this.setState({ page : i })
  }

  matchImage (p) {
    this.setState({ action : 'matching' });
    let that = this;
    request
      .post('/match')
      .send({ path : p})
      .set('Accept', 'application/json')
      .end(function(err, res){
        console.log("got response");
        console.log(res);
        if (err || !res.ok) {
          alert('Oh no! error');
        }
        else {
          let result = JSON.parse(res.text);
          let path = result.path;
          let score = result.score;
          let time = result.time;
          let correspondence = result.correspondence;
          console.log("correspondence");
          console.log(correspondence);
          that.setState({action : 'matched', matched : { path : path, score : score, time : time }, correspondence : correspondence });
        }
      });
  }

  updateStats (accurate) {
    let that = this;
    let time = this.state.matched.time;
    request
      .post('/matchquality')
      .send({ accurate : accurate, time : time })
      .set('Accept', 'application/json')
      .end(function (err, res) {
        that.clear();
        console.log("correspondence updateStats");
        console.log(that.state.correspondence);
        that.props.complete(that.state.correspondence);
      });

  }

  clear () {
    this.setState({ action : 'none', matched : null  });
  }

  render () {
    var content;

    if(this.state.action == 'none') {
      content = <div>
                  <h3>Selected Image</h3>
                  <img src={'/images/test/' + this.props.image} />
                  <Button text={'Find Match'} click={(e) => this.matchImage(this.props.image)} />
                  <Button text={'Select Other'} click={(e) => this.props.back(e)} />
                </div>
    }
    if(this.state.action == 'matching') {
      content = <div>
                  <table>
                    <tr>
                      <td><h3>Test Image</h3></td>
                      <td></td>
                      <td><h3>Matched Image</h3></td>
                    </tr>
                    <tr>
                      <td><img src={'/images/test/' + this.props.image} /></td>
                      <td><i className='fa fa-circle-o-notch fa-spin'></i></td>
                      <td></td>
                    </tr>
                  </table>
                </div>
    }
    if(this.state.action == 'matched') {
      content = <div>
                  <table>
                    <tr>
                      <td><h3>Test Image</h3></td>
                      <td></td>
                      <td><h3>Matched Image</h3></td>
                    </tr>
                    <tr>
                      <td><img src={'/images/test/' + this.props.image} /></td>
                      <td><i className='fa fa-check'></i></td>
                      <td><img src={'/images/reference/' + this.state.matched.path} /></td>
                    </tr>
                  </table>
                  <table>
                    <tr>
                      <td><h3>Was this a good match?</h3></td>
                    </tr>
                    <tr>
                      <td>
                        <Button text={'Yes'} click={(e) => this.updateStats(true)} />
                        <Button text={'No'}  click={(e) => this.updateStats(false)}/>
                      </td>
                    </tr>
                  </table>
                </div>
    }


    return (
      <div className='imagematching'>
        {content}
      </div>
    );
  }
}

export default ImageMatching;
