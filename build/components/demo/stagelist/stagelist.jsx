import React from 'react';

class StageList extends React.Component {

  render () {
    return (
        <div className='stagelist'>
          {this.props.children}
        </div>
    );
  }
}

export default StageList;
