import React from 'react';

class Stage extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    var status_classname = 'fa fa-times';
    if(this.props.complete)
      status_classname = 'fa fa-check';

    var content_classname = 'stage-content';
    if(this.props.active)
      content_classname = 'stage-content active';

    return (
      <div className='stage'>
        <div className='summary' onClick={this.props.click}>
          <p className='number'>{this.props.number}</p>
          <p className='title'>{this.props.title}</p>
          <i className={status_classname}></i>
        </div>
        <div className={content_classname}>
          { this.props.children }
        </div>
      </div>
    );
  }
}

Stage.defaultProps = { number : 1, title : 'Default', active : false, complete : false };

export default Stage;
