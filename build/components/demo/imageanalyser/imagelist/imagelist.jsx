import React from 'react';
import Image from '../image.jsx';
import Patch from '../patch.jsx';

class ImageList extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    let images = [];

    console.log("xnn");
    console.log(this.props.xnn);

    if(typeof this.props.xnn != 'undefined') {
      let xnn_length = Object.keys(this.props.xnn).length
      for(var i=0; i<xnn_length; i++) {
        let x = this.props.xnn[i];
        let filename = x.filename.split("/").pop();
        images.push(<Image url={'/images/reference/' + filename}><Patch left={x.column*4} top={x.row*4} /></Image>);
      }
    }

    return (
      <div className='image-list'>
        {images}
      </div>
    );
  }
}

export default ImageList;
