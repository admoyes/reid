import React from 'react';

class ControlButton extends React.Component {
  render () {
    let buttonStyle;

    switch(this.props.position) {
      case 'top':
        buttonStyle = { top : '0px', left : '33%' };
        break;
      case 'left':
        buttonStyle = { top : '33%', left : '0px' };
        break;
      case 'right':
        buttonStyle = { top : '33%', right : '0px' };
        break;
      case 'bottom':
        buttonStyle = { bottom : '0px', left : '33%' };
        break;
      case 'center':
        buttonStyle = { top : '33%', left : '33%' };
        break;
    }

    return (
      <div className='control-button' onClick={this.props.click} style={buttonStyle}>
        <i className={this.props.text}/>
      </div>
    );
  }
}

export default ControlButton;
