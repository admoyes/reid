import React from 'react';
import Patch from './patch.jsx';
import Image from './image.jsx';
import Controls from './controls.jsx';
import ControlButton from './controlbutton.jsx';
import ImageList from './imagelist/imagelist.jsx';
import Button from '../../button/button.jsx';

class ImageAnalyser extends React.Component {
  constructor (props) {
    super(props);
    this.state = { left : 0, top : 0, xnn : [] };
  }

  componentDidMount () {
    window.addEventListener('keyup', (e) => {
      switch (e.keyCode) {
        case 65:
          this.arrowClick('left');
          break;
        case 87:
          this.arrowClick('top');
          break;
        case 68:
          this.arrowClick('right');
          break;
        case 83:
          this.arrowClick('bottom');
          break;
      }
    }, true);
    if(typeof this.props.correspondence != 'undefined') {
      let xnn = this.props.correspondence.patches[0][0].xnn;
      this.setState({ xnn : xnn });
    }

  }

  componentWillUnmount () {
    window.removeEventListenter('keyup');
  }

  arrowClick (dir) {
    let left = this.state.left;
    let top = this.state.top;
    switch(dir) {
      case 'left':
        left -= 4;
        break;
      case 'right':
        left += 4;
        break;
      case 'top':
        top -= 4;
        break;
      case 'bottom':
        top += 4;
        break;
    }
    if(left < 0)
      left = 0;
    if(left > 36)
      left = 36
    if(top < 0)
      top = 0
    if(top > 116)
      top = 116
    let current_patch = this.props.correspondence.patches[top/4][left/4];
    this.setState({ left : left, top : top, xnn : current_patch.xnn });
  }

  render () {
    let correspondence =  this.props.correspondence;
    if(typeof correspondence == 'undefined') {
      correspondence = {};
      correspondence.patches = [];
      correspondence.filename = '';
    }
    let filename = '/images/test/' + correspondence.filename.split("/").pop();
    return (
      <div className='image-analyser'>
        <h1>Analysis</h1>
        <p>Use the controls below (or the <b>WASD</b> keys) to see the best matchs between the selected patch on the test image and all other images in the reference set</p>
        <div className='flex-row'>
          <Image url={filename}>
            <Patch left={this.state.left} top={this.state.top} />
          </Image>
          <Controls>
            <ControlButton text={'fa fa-arrow-left'} position={'left'} click={(e) => this.arrowClick('left')} />
            <ControlButton text={'fa fa-arrow-up'} position={'top'} click={(e) => this.arrowClick('top')} />
            <ControlButton text={'fa fa-arrow-right'} position={'right'} click={(e) => this.arrowClick('right')} />
            <ControlButton text={'fa fa-arrow-down'} position={'bottom'} click={(e) => this.arrowClick('bottom')} />
            <ControlButton text={'fa fa-circle'} position={'center'} />
          </Controls>
        </div>
        <h1>Image List</h1>
        <ImageList xnn={this.state.xnn} />
        <div className='t-align-center'>
          <Button text={'Try Again'} />
        </div>
      </div>
    );
  }
}

export default ImageAnalyser;
