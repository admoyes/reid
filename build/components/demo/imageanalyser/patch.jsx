import React from 'react';

class Patch extends React.Component {
  render () {
    let patchStyle = { left : String(this.props.left) + 'px', top : String(this.props.top) + 'px' };
    return (
      <div className='patch' style={patchStyle}>
      </div>
    );
  }
}

export default Patch;
