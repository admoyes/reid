import React from 'react';

class Image extends React.Component {
  render () {
    let imageStyle = { backgroundImage : "url('" + this.props.url + "')" };

    return (
      <div className='image' style={imageStyle}>
        {this.props.children}
      </div>
    );
  }
}

export default Image;
