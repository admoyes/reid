import React from 'react';
import Image from './image/image.jsx';

class ImageSelector extends React.Component {

  render () {
    var that = this;
    return (
      <div className='imageselector'>
        {this.props.images.map(function(img, i) {
          return <Image url={img} key={i} click={that.props.complete} />;
        })}
      </div>
    );
  }

}

ImageSelector.defaultProps = { images : [] };

export default ImageSelector;
