import React from 'react';

class Image extends React.Component {
  render () {
    var img_style = {
        backgroundImage : "url('/images/test/" + this.props.url + "')"
    };
    return (
      <div className='image-container'>
        <div className='image' style={img_style} onClick={(e) => this.props.click(this.props.url)}></div>
      </div>
    );
  }
}

export default Image;
