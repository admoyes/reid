import React from 'react';
import ToolbarItem from '../../toolbar/toolbaritem.jsx';
import Toolbar from '../../toolbar/toolbar.jsx';
import ImageSelector from './imageselector/imageselector.jsx';
import ImageUploader from './imageuploader/imageuploader.jsx';
import request from 'superagent';

class ImageManager extends React.Component {
  constructor (props) {
    super(props);
    this.state = { page : 0, filename: '', test_images : []};
  }

  componentDidMount () {
      this.getTestImages();
  }

  getTestImages () {
    let that = this;
    request
      .get('/test_list')
      .end(function(err, res){
        if(err || !res.ok)
          alert(err);
        else {
          console.log(res);
          that.setState({ test_images : res.body.files });
        }
      });
  }

  setActivePage (i) {
    this.setState({ page : i });
  }

  uploadComplete (e) {
    this.setState({ filename : e });
    let test_images = this.state.test_images;
    test_images.push(e);
    this.setState({ test_images : test_images });
    this.props.complete(e);
  }

  imageSelected (e) {
    this.props.complete(e);
  }

  render () {
    if(this.state.page==0)
      var content = <ImageSelector images={this.state.test_images} complete={(e) => this.imageSelected(e)} />;
    if(this.state.page==1)
      var content = <ImageUploader complete={(e) => this.uploadComplete(e)} />;

    return (
      <div className='imagemanager'>
        <Toolbar>
          <ToolbarItem icon={'fa fa-th'} text={'Existing'} selected={this.state.page==0} click={(e) => this.setActivePage(0)} />
          <ToolbarItem icon={'fa fa-upload'} text={'New'} selected={this.state.page==1} click={(e) => this.setActivePage(1)} />
        </Toolbar>
        {content}
      </div>
    );
  }
}

export default ImageManager;
