import React from 'react';
import FileUploader from './fileuploader/fileuploader.jsx';

class ImageUploader extends React.Component {
  render () {
    return (
      <div className='imageuploader'>
        <FileUploader complete={this.props.complete} />
      </div>
    );
  }
}

export default ImageUploader;
