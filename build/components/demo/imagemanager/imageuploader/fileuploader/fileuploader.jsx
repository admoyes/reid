import React from 'react';
import request from 'superagent';

class FileUploader extends React.Component {
  constructor (props) {
    super(props);
    this.state = { filename : '', status: 'no-file'};
  }

  filechange (e) {
    this.setState({ filename : e.target.files[0].name });

    // Create new FormData
    let formData = new FormData();
    // Add the file to formData
    formData.append('image', e.target.files[0]);
    // Call uploadfile
    this.uploadfile(formData);
  }

  uploadfile (formData) {
    let that = this;
    request
      .post('http://192.168.0.18:3000/test_upload')
      .send(formData)
      .set('Accept', 'application/json')
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Oh no! error');
        } else {
          that.setState({ status : 'got-file' });
          that.props.complete(res.body.filename);
        }
      });
  }

  render () {
    let filename_content = 'Click here to upload';
    if(this.state.filename.length > 0)
      filename_content = this.state.filename;

    // Handle icon
    if(this.state.status == 'no-file')
      var i_classname = 'fa fa-upload';
    if(this.state.status == 'uploading')
      var i_classname = 'fa fa-circle-o-notch fa-spin';
    if(this.state.status == 'got-file')
      var i_classname = 'fa fa-check'

    return (
      <div className='fileuploader'>
        <input type='file' id='image' name='image' onChange={(e) => this.filechange(e)} />
        <label htmlFor='image'>
          <p>{filename_content}</p>
          <p className='btn'><i className={i_classname}></i></p>
        </label>
      </div>
    );
  }
}

export default FileUploader;
