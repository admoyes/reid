import React from 'react';
import Stage from './stagelist/stage/stage.jsx';
import StageList from './stagelist/stagelist.jsx';
import ToolbarItem from '../toolbar/toolbaritem.jsx';
import Toolbar from '../toolbar/toolbar.jsx';
import ImageManager from './imagemanager/imagemanager.jsx';
import ImageMatching from './imagematching/imagematching.jsx';
import ImageAnalyser from './imageanalyser/imageanalyser.jsx';

class Demo extends React.Component {
  constructor (props) {
      super(props);
      this.state = { stage : 0, selected_image : '', matched : false };
  }

  imageSelected (e) {
    this.setState({ stage : 1, selected_image : e });
  }

  selectNewImage () {
    this.setState({ stage : 0, selected_image : '' });
  }

  setActiveStage (i) {
    this.setState({ stage : i });
  }

  getCorrespondence (correspondence) {
    if(correspondence != null) {
      this.setState({ correspondence : correspondence, matched : true, stage : 2 });
    }
    else {
      alert("error");
    }
  }

  render () {
    return (
      <div className='content overflow-y'>
        <StageList>
          <Stage number={1} title={'Select Target Image'} complete={this.state.selected_image.length > 0} active={this.state.stage==0} click={(e) => this.setActiveStage(0)}>
            <ImageManager complete={(e) => this.imageSelected(e)} />
          </Stage>
          <Stage number={2} title={'Matching'} complete={this.state.matched} active={this.state.stage==1} click={(e) => this.setActiveStage(1)}>
            <ImageMatching image={this.state.selected_image} back={(e) => this.selectNewImage(e)} complete={(e) => this.getCorrespondence(e)} />
          </Stage>
          <Stage number={3} title={'Analysis'} complete={false} active={this.state.stage==2} click={(e) => this.setActiveStage(2)}>
            <ImageAnalyser correspondence={this.state.correspondence} />
          </Stage>
        </StageList>
      </div>
    );
  }
}

export default Demo;
