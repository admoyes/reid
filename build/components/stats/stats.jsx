import React from 'react';
import { Pie } from 'react-chartjs';

class Stats extends React.Component {

    constructor(props) {
      super(props);
    }

    render() {
      var chartData = [
        {
            value : this.props.accuracy,
            color : "#2c9c69"
        },
        {
            value : 100-this.props.accuracy,
            color : "#c62f29"
        }
      ];
      var chartOptions = {};
      var color = '';

      if(this.props.accuracy < 45)
        color = 'red';
      if(this.props.accuracy >= 45 && this.props.accuracy < 65)
        color = 'orange';
      if(this.props.accuracy >= 65)
        color = 'green';


      return (
        <div className='content overflow-y'>
          <div className='stat t-align-center'>
            <Pie data={chartData} width="200" height="200"/>
            <h1 className={color} >{this.props.accuracy}% Correct</h1>
            <p>On average</p>
          </div>
          <div className='stat t-align-center'>
            <h1>{this.props.time} Seconds</h1>
            <p>per request on average</p>
          </div>
          <div className='stat t-align-center'>
            <h1>{this.props.requests} Requests</h1>
            <p>So far</p>
          </div>
        </div>
      );
    }
}

Stats.defaultProps = { accuracy : 65, time : 25 , requests : 150}

export default Stats;
