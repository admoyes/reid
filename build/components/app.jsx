import React from 'react';
import {render} from 'react-dom';
import Toolbar from './toolbar/toolbar.jsx';
import ToolbarItem from './toolbar/toolbaritem.jsx';
import Homepage from './homepage/homepage.jsx';
import Stats from './stats/stats.jsx';
import Demo from './demo/demo.jsx';

class App extends React.Component {
	constructor (props) {
		super(props);
		this.state = { page : 0 };
	}

	setActivePage (i) {
		this.setState({ page : i});
	}

	render () {
		var content;

		// 0 : Homepage
		if(this.state.page == 0)
			content = <Homepage />;
		if(this.state.page == 1)
			content = <Stats />;
		if(this.state.page== 2)
			content = <Demo />;

		return (
			<div className='app'>
				<div className='title'>
					<i className="fa fa-eye"></i>
					<p>REID</p>
				</div>
				{content}
				<Toolbar>
					<ToolbarItem icon={'fa fa-home'} text={'Home'} selected={this.state.page==0} click={(e) => this.setActivePage(0)} />
					<ToolbarItem icon={'fa fa-line-chart'} text={'Statistics'} selected={this.state.page==1} click={(e) => this.setActivePage(1)} />
					<ToolbarItem icon={'fa fa-rocket'} text={'Demo'} selected={this.state.page==2} click={(e) => this.setActivePage(2)} />

				</Toolbar>
			</div>
		);
	}
}

render(<App/>, document.getElementById('mount-point'));
