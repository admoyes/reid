var mongoose = require('mongoose');
var Stats = mongoose.model('Stats');

exports.init_stats = function init_stats() {
  console.log("Initialising stats");
  var default_stats = new Stats({ totalRequests : 0, correctRequests : 0, lastTimes: [] });
  Stats.count({}, function(err, c) {
    console.log("Got " + c.toString() + " stats");
    if(c == 0) {
      default_stats.save(function(err) {
        console.log("Inserted default stats");
        console.log("Finished initialising");
      });
    }
    else {
      console.log("Stats already exists. Skipping");
      console.log("Finished initialising");
    }
  });
}

exports.get_stats = function get_stats(callback) {
  Stats.find({}, function(err, data) {
    callback(err, data);
  });
}

exports.add_request = function add_request(value, time, callback) {
  Stats.find({}, function(err, data) {
    if(err) {
      console.log("Could not add request. Got error");
      console.log(err);
    }
    else {
      console.log("Got stats. Adding request.");
      console.log("Accurate: " + value.toString() + " Time: " + time.toString());
      if(typeof data.lastTimes === 'undefined')
        data.lastTimes = [];
      if(data.lastTimes.length == 5)
        data.lastTimes.shift();
      data.lastTimes.push(time);
    }
    data.totalRequests += 1;
    if(value) // request was Accurate
      data.correctRequests += 1;
    console.log("Saving updated stats");
    console.log(data);
    data.save(function(err) {
      callback(err, data);
    })
  });
}
