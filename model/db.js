var mongoose = require('mongoose');

/* Schemas */

var statSchema = new mongoose.Schema({
	totalRequests : Number,
	correctRequests : Number,
	lastTimes : Array
});

mongoose.model('Stats', statSchema);
console.log("Attempting to connect to mongodb://127.0.0.1/reid");
mongoose.connect('mongodb://127.0.0.1/reid');
console.log("Connected to mongodb://127.0.0.1/reid");

// Initialise db
statdata = require('./stats');
statdata.init_stats();
